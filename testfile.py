import json

import pytest
from * import main_lambda
from hamcrest import assert_that, contains, equal_to, has_entries
from pytest_elasticsearch import factories

elasticsearch_proc = factories.elasticsearch_proc(port=-1, index_store_type='fs')
elasticsearch = factories.elasticsearch('elasticsearch_proc')class TestSuccessResponses:    @pytest.fixture(autouse=True)
    def spam_index(self, elasticsearch):
        elasticsearch.indices.create(index='spam')
        elasticsearch.indices.put_mapping(body={
            'properties': {
                'id': {
                    'type': 'keyword'
                },
                'type': {
                    'type': 'text',
                    'fields': {
                        'keyword': {
                            'type': 'keyword',
                            'ignore_above': 256
                        }
                    }
                }
            }
        }, doc_type='_doc', index='spam')    def test_egg_finder(self, elasticsearch):
        elasticsearch.create('spam', '1', {
            'id': '1',
            'type': 'Scrambled'}, refresh=True)        test_event = {
            'multiValueQueryStringParameters': {
                'type': ['scrambled']
            }
        }        actual_response = main_lambda.handle_event(test_event, elasticsearch, 'spam')
        assert_that(actual_response, has_entries({
            'statusCode': equal_to(200),
            'statusDescription': equal_to('200 OK'),
            'isBase64Encoded': equal_to(False),
            'multiValueHeaders': equal_to({'Content-Type': ['application/json;charset=utf-8']})
        }))
        assert_that(json.loads(actual_response.get('body')), contains(has_entries({
            'id': equal_to('1'),
            'type': equal_to('Scrambled')
        })))
